/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.ufps.colecciones_seed;

/**
 *  Nuestra primera estructura de datos : En seed la consiguen como Secuencia
 * https://gitlab.com/estructuras-de-datos/proyecto-seed
 * @author madar
 */
public class VectorGenerico<T> {
    
    private T []vector;
    private int posActual=0;
    

    public VectorGenerico() {
    }
    
    
    public VectorGenerico(int capacidad) {
        
        if (capacidad <=0)
            throw new RuntimeException("No se pueden crear vector con capacidad 0 o negativa");
        //Tips:
        // Crear a partir de object
        //T --> Object
        this.vector=(T[])new Object[capacidad];
    }
    
    public void add(T datoNuevo)
    {
        if(this.posActual>=this.length())
            throw new RuntimeException("No se puede insertar más elementos, llegó a su límite");
        this.vector[this.posActual]=datoNuevo;
        this.posActual++;
    
    }
    
    
    public T get(int indice)
    {
        if(indice<0 || indice >=this.vector.length)
            throw new RuntimeException("Índice fuera de rango");
            
        return this.vector[indice];
    }
    
    
    public void set(int indice, T datoNuevo)
    {
    /*
        abierto:
        if(indice<0 || indice >=this.vector.length)
            throw new RuntimeException("Índice fuera de rango");
      */
        
        if(indice<0 || indice >=this.posActual)
            throw new RuntimeException("Índice fuera de rango");
    this.vector[indice]=datoNuevo;
    }    
    public int length()
    {
        return this.vector.length;
    }
    

    @Override
    public String toString() {
        String msg="";
        for(int i=0;i<this.vector.length;i++)
        {
                if(this.vector[i]!=null)
                    msg+= this.vector[i].toString()+"\t";
        }
        return msg;
    }
    
    
    public void  sort() {

        int j = 0;
        for (int i = 1; i < vector.length; i++) {
            T clave = vector[i];
            j = i - 1;
            while (j >= 0 && (((Comparable)vector[j]).compareTo(clave) == 1)) {
                T aux = vector[j + 1];
                vector[j + 1] = vector[j];
                vector[j] = aux;
                j = j - 1;
            }

        }
    }
    
}
